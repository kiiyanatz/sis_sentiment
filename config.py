import os

# Project's root directory
_basedir = os.path.abspath(os.path.dirname(__file__))

class Default:
    PORT = 1738

class Development(Default):
    DEBUG = True

class Production(Default):
    pass

class Testing(Default):
    TESTING = True

config = {
    'DEFAULT': Default,
    'DEVELOPMENT': Development,
    'PRODUCTION': Production,
    'TESTING': Testing
}