"""Tweets module.

Streams and saves live tweets to mongodb
"""
import time
from pymongo import MongoClient
import tweepy
from tweepy import Stream, StreamListener
import json
from app.twitter import twitter as twitter_bp
from flask_socketio import send, emit
from threading import Thread



# Setup access keys
consumer_key = 'lAmZHbqErBLg6EXlhS0ZJu49w'
consumer_secret = '87r7AUFoG3DzrgZ4UQSyBBrpAuH4PUX52ylyVDGJ5GPBCAr06n'
access_token = '3665986227-8P479RIa7LVgAvJstP5qcpwsgDpnLalYvjUVG67'
access_token_secret = 'OyMqIp5DC7OC6awllfWUW9BbGtWLyrsh8FbRmF5UMH57X'

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)

api = tweepy.API(auth)

thread = None

class SisStreamListener(StreamListener):
    """Create tweet streamer.

    Creates a tweets streamer object that is used to save
    new tweets to db
    """

    def on_data(self, data):
	""" Save tweets to  db """
 
	client = MongoClient('localhost', 27017)
	db = client.sis_sentiment_sys
	tweets_collection = db.tweets
	tweet = json.loads(data)
	tweets_collection.insert(tweet)
	

sis_stream_listener = SisStreamListener()
sis_stream = Stream(auth=api.auth, listener=sis_stream_listener)

def save_twts(search_query=None):
    sis_stream.filter(track=[search_query])

@twitter_bp.route('/stream/<q>')
def stream_index(q):
    global thread
    if thread is None:
        thread = Thread(target=save_twts(q))
	thread.start()
    return "<h1>Streaming tweets</h1>"
