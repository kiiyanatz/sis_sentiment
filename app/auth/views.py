from app.auth import auth
from flask import render_template, request


@auth.route('/login', strict_slashes=False)
def login():
    return render_template('signin_signup.html')
