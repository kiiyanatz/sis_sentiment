from flask import Blueprint

auth = Blueprint(
    'auth', __name__,
    static_folder='auth_static',
    template_folder='auth_templates')

# Import auth modules here
from app.auth import views
