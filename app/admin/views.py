from flask import render_template
from . import admin


@admin.route('/admin', strict_slashes=False)
def admin_home():
    return render_template('admin.html')