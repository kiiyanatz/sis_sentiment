from flask import Flask
from config import config


# Create app
def create_app(environment):
    '''
    Creates flask application and loads application components/blueprints

    :param environment: The environment to run app on (development, debug and testing)
    :return: Returns the created application
    '''

    app = Flask(
        __name__, static_folder='./static', template_folder='./templates')
    app.config.from_object(config[environment])

    # Import application components/blueprints
    from app.core import core as core_blueprint
    from app.admin import admin as admin_blueprint
    from app.auth import auth as auth_blueprint
    from app.twitter import twitter as twitter_blueprint

    # Register blueprints
    app.register_blueprint(core_blueprint)
    app.register_blueprint(admin_blueprint)
    app.register_blueprint(auth_blueprint, url_prefix='/auth') 
    app.register_blueprint(twitter_blueprint)
    #print(app.url_map)

    return app
