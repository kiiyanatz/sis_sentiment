# README #

This is a simple flask application that can be used as a template to create flask applications

### Features ###

* This applications utilizes flask blueprints to organize a project and make it more moduler


### How do I get set up? ###

* Install pip if required on linux
```
$ sudo apt-get install python pip
```
* Configuration
Creating virtual environment
clone project from git using git clone http://project/url/name.git
```
$ cd project_folder
$ virtualenv env
$ . env/bin/activate
$ pip install flask
```